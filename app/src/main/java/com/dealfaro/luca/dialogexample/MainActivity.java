package com.dealfaro.luca.dialogexample;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void eraseButton(View v) {
        // Let's show a confirmation dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.erase_confirm_title);
        builder.setMessage(R.string.erase_confirm_msg);

        builder.setPositiveButton(R.string.confirm_ok,
                new AlertDialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, R.string.erased_all, Toast.LENGTH_SHORT).show();
                    }
                });

        builder.setNegativeButton(R.string.confirm_no,
                new AlertDialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, R.string.kept_all, Toast.LENGTH_SHORT).show();
                    }
                });
        // Shows the dialog.
        builder.show();

    }

}
